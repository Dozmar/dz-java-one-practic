package three.project.array;

import java.util.Arrays;

public class MainThree {

    public static void main(String[] args) {
        double arr[] ={5, 7, 12, 85, 24, 32 , 67, 81, 25};
        for (int i = 0; i < arr.length; i++) {
            arr[i] += arr[i] * 0.1;
        }
        for ( int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++ ) {
                if (arr[j] < arr[j + 1]) {
                    double tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        String arrString = Arrays.toString(arr);
        System.out.println(arrString);
    }
}
