package six.project.supreamitzm;

public class MainSix {

    static Figure[] figures = new Figure[5];

    public static void main(String[] args) {


        figures[0] = new Circle(-3, 5, "Круг");
        figures[1] = new Rectangle(2, 3, "Прямоугольник");
        figures[2] = new Circle(2, 3, "Круг");
        figures[3] = new Rectangle(2, 3, "Прямоугольник");
        figures[4] = new Circle(2, 3, "Круг");
        for (int i = 0; i < 5; i++) {
            System.out.print(figures[i].getName());
            figures[i].square();
        }

    }
}
