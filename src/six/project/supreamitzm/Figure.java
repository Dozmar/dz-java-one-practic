package six.project.supreamitzm;

public abstract class Figure {
    private String name;
    private int x;
    private int y;
    private int quarter;


    public Figure(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }


    public abstract void square();

    public void getQuadrant(int x, int y) {
        if (x > 0 && y > 0) {
            quarter = 1;
        } else if (x < 0 && y > 0) {
            quarter = 2;
        } else if (x < 0 && y < 0) {
            quarter = 3;
        } else if (x > 0 && y < 0) {
            quarter = 4;
        }
        System.out.println("Четверть равна: " + quarter);
    }

    public int getX() {
        return x;
    }

    public void setX() {
        this.x = x;
    }

    public int getQuarter() {
        return quarter;
    }

    public int getY() {
        return y;
    }

    public void setY() {
        this.y = y;
    }
    public String getName() {
        return name;
    }
    public void setName() {
        this.name = name;
    }
}