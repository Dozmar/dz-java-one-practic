package six.project.supreamitzm;


public class Rectangle extends Figure {

    private double a;
    private double b;

    public Rectangle(int x, int y,String name) {
        super(x, y, name);

    }


    @Override
    public void square() {
        int first = 1;
        int last = 20;
        a = first + (int) (Math.random()* last);
        b = first + (int) (Math.random()* last);
            System.out.println(" площадь равна: " + (a * b));
    }


    public double getA() {
        return a;
    }

    public void setA() {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB() {
        this.b = b;
    }


}
