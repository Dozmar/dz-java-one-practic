package six.project.supreamitzm;


public class Circle extends Figure {
    private double r;

    public Circle(int x, int y, String name) {
        super(x, y, name);
    }



    @Override
    public void square() {
        int first = 1;
        int last = 10;
        double R = first + (int) (Math.random()* last);
            System.out.println(" площадь равна: " + Math.pow(R,2) * Math.PI);
    }


    public double getR() {
        return r;
    }

    public void setR() {
        this.r = r;
    }

}