package five.project.recursive;

import java.util.Scanner;

public class MainFive {

        public static void main(String[] args) {
            System.out.println("Введите строку");
            Scanner in = new Scanner(System.in);
            String s = in.nextLine();

            if(s.length() > 0) {
                reverseString(s, s.length() - 1);
            }
        }

        public static void reverseString(String s, int index) {
            if(index == 0) {
                System.out.println(s.charAt(index));

                return;
            }
            System.out.print(s.charAt(index));

            reverseString(s, index - 1);
        }

}
