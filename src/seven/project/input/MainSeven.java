package seven.project.input;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Collectors;

public class MainSeven {


    private final static Random RANDOM = new Random();

    public static void main(String[] args) throws IOException {

        String[] fileNames = {"C://SE2020_LESSON9/filename_", "C://SE2020_LESSON9/Directory_1/filename_",
                "C://SE2020_LESSON9/Directory_1/Directory_2/filename_", "C://SE2020_LESSON9/Directory_1/Directory_2/Directory_3/filename_"};
        String[] dirNames = {"C://SE2020_LESSON9/", "C://SE2020_LESSON9/Directory_1/",
                "C://SE2020_LESSON9/Directory_1/Directory_2", "C://SE2020_LESSON9/Directory_1/Directory_2/Directory_3"};

        int randomRange = random(1, 3);
        for (int i = 0; i < randomRange; i++) new File(dirNames[i]).mkdirs();
        for (int i = 0; i < randomRange; i++) square(fileNames[i]);
    }

    private static void square(String fileName) throws IOException {
        for (int i = 1; i < 4; i++) {
            byte[] content = RANDOM.ints(random(10, 200), 0, 9)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(" ")).getBytes();
            Files.write(Paths.get(fileName + i + ".txt"), content);
        }
    }

    private static int random(int min, int max) {
        return RANDOM.nextInt(max-min+1)+min;
    }
}



